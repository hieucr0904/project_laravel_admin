$("document").ready(function () {
    getList();
});

function callCategoryApi(url, data = {}, method = "GET")
{
    return $.ajax({
        url: url,
        data: data,
        type: method,
    })
}

function getListBy(url)
{
    let name = $('#name').val();
    let parent_id = $('#parent_id').val();
    let data = {
        name: name,
        parent_id: parent_id,
    }

    callCategoryApi(url, data)
        .then(function (res) {
            $('#list').replaceWith(res);
        })
}

function getList()
{
    let url = $('#list').data('action');
    getListBy(url);
}

$('#name').on('keyup', function () {
    getList();
})

function afterCallApiSuccess(res)
{
    getList();
    $('#success_message').show();
    $('#success_message').addClass('alert alert-success');
    $('#success_message').text(res.message);
    $('#success_message').hide(3000);
}

$('.btn-create').on('click', function (e) {
    e.preventDefault();
    let url = $('.btn-create').data('action');
    let data = {
        name: $('#nameCreate').val(),
        parent_id: $('#parentIdCreate').val(),
    };

    let method = "POST";
    callCategoryApi(url, data, method)
        .done(function (res) {
            afterCallApiSuccess(res);
            $('#CreateCategoryModal').modal('hide');
            $('#CreateCategoryModal').find('input').val("");
        })
        .fail(function (res) {
            $.each(res.responseJSON.errors, function (k, v) {
                $('span.' + k + '_error_create').text(v[0]);
            })
        })
})

$(document).on('click', '.page-link', function (e) {
    e.preventDefault();
    let url = $(this).attr('href');
    getListBy(url);
})

let urlUpdate = '';

$(document).on('click', '.btn-edit', function (e) {
    e.preventDefault();
    let url = $(this).data('action');
    urlUpdate = $(this).data('update');
    let data = {};
    let method = "Get";
    callCategoryApi(url, data, method)
        .then(function (res) {
            $('#cID').val(res.category.id);
            $('#nameUpdate').val(res.category.name);
            $('#parentIdUpdate').val(res.category.parent_id ?? '');
        })
})

$(document).on('click', '.btn-update', function (e) {
    e.preventDefault();
    let data = {
        name: $('#nameUpdate').val(),
        parent_id: $('#parentIdUpdate').val(),
    };
    let method = "put";
    callCategoryApi(urlUpdate, data, method)
        .done(function (res) {
            afterCallApiSuccess(res);
            $('#EditCategoryModal').modal('hide');
            $('#EditCategoryModal').find('input').val("");
        })
        .fail(function (res) {
            $.each(res.responseJSON.errors, function (k, v) {
                $('span.' + k + '_error_update').text(v[0]);
            })
        })
})

// function deleteCategory(id) {
//     Swal.fire({
//         title: 'Are you sure?',
//         text: "You won't be able to revert this!",
//         icon: 'warning',
//         showCancelButton: true,
//         confirmButtonColor: '#3085d6',
//         cancelButtonColor: '#d33',
//         confirmButtonText: 'Yes, delete it!'
//     }).then((result) => {
//         if (result.isConfirmed) {
//             let url = `/categories/${id}`;
//             let data = {};
//             let method = "DELETE";
//             callCategoryApi(url, data, method)
//                 .then(function () {
//                     getList();
//                 })
//             Swal.fire(
//                 'Deleted!',
//                 'Your file has been deleted.',
//                 'success'
//             )
//         }
//     })
// }

$('#CreateCategoryModal').on('hide.bs.modal', function (e) {
    $(this).find("input").val('').end()
        .find("span").text('').end()
        .find("option[value='0']").prop("selected", true).end();
})

$('#EditCategoryModal').on('hide.bs.modal', function (e) {
    $(this).find("input").val('').end()
        .find("span").text('').end()
        .find("option[value='0']").prop("selected", true).end();
})


