<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DeleteCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_and_has_permission_user_can_delete_category()
    {
        $category = Category::factory()->create();
        $this->loginUserWithPermission('category_delete');
        $response = $this->json('DELETE', '/categories/' . $category->id);
        $response->assertStatus(Response::HTTP_OK);
        $response
            ->assertJson(fn(AssertableJson $json) => $json->where('status', Response::HTTP_OK)
                ->where('message', 'Data delete succesfully!')
                ->etc()
            );
    }

    /** @test */
    public function authenticated_super_admin_can_delete_category()
    {
        $category = Category::factory()->create();
        $this->loginUserWithPermission('category_delete');
        $response = $this->json('DELETE', '/categories/' . $category->id);
        $response->assertStatus(Response::HTTP_OK);
        $response
            ->assertJson(fn(AssertableJson $json) => $json->where('status', Response::HTTP_OK)
                ->where('message', 'Data delete succesfully!')
                ->etc()
            );
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_category()
    {
        $category = Category::factory()->create();
        $response = $this->json('DELETE', '/categories/' . $category->id);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /** @test */
    public function unauthenticated_super_admin_can_not_delete_category()
    {
        $category = Category::factory()->create();
        $response = $this->json('DELETE', '/categories/' . $category->id);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }
}
