<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class UpdateRoleTest extends TestCase
{
    /** @test */
    public function super_admin_can_see_edit_role_form()
    {
        $this->loginWithSuperAdmin();
        $role = Role::factory()->create();
        $response = $this->get(route('roles.edit', $role->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.roles.edit');
        $response->assertSee('name')->assertSee('display_name');
    }

    /** @test */
    public function super_admin_can_update_role()
    {
        $this->loginWithSuperAdmin();
        $role = Role::factory()->create();
        $data = [
            'name' => 'hieu',
            'display_name' => 'hieu12',
        ];
        $response = $this->put(route('roles.update', $role->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('roles', $data);
    }

    /** @test */
    public function super_admin_can_not_update_role_if_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $role = Role::factory()->create();
        $data = [
            'name' => null,
            'display_name' => 'hieu12'
        ];
        $response = $this->put(route('roles.update', $role->id), $data);
        $response->assertSessionHasErrors(['name']);
        $this->assertDatabaseMissing('roles', $data);
    }

    /** @test */
    public function authenticated_user_have_permission_can_see_edit_form()
    {
        $this->loginUserWithPermission('role_edit');
        $role = Role::factory()->create();
        $response = $this->get(route('roles.edit', $role->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.roles.edit');
        $response->assertSee('name')->assertSee('display_name');
    }

    /** @test */
    public function authenticated_user_have_permission_can_update_role()
    {
        $this->loginUserWithPermission('role_edit');
        $role = Role::factory()->create();
        $data = [
            'name' => 'hieu',
            'display_name' => 'hieu12',
        ];
        $response = $this->put(route('roles.update', $role->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('roles', $data);
    }

    /** @test */
    public function authenticated_user_have_permission_can_not_update_role_if_name_is_null()
    {
        $this->loginUserWithPermission('role_edit');
        $role = Role::factory()->create();
        $data = [
            'name' => null,
            'display_name' => 'hieu12'
        ];
        $response = $this->put(route('roles.update', $role->id), $data);
        $response->assertSessionHasErrors(['name']);
        $this->assertDatabaseMissing('roles', $data);
    }

    /** @test */
    public function unauthenticated_user_can_not_see_edit_role_form()
    {
        $role = Role::factory()->create();
        $response = $this->get(route('roles.edit', $role->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}
