<?php

namespace App\Repositories;

use App\Models\Category;

class CategoryRepository extends BaseRepository
{
    public function model()
    {
        return Category::class;
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['name'])->latest('id')->paginate(10);
    }

    public function getCategoryGroup()
    {
        return $this->model->all()->groupBy('id');
    }

    public function getParentId()
    {
        return $this->model->all()->where('parent_id', null);
    }
}
