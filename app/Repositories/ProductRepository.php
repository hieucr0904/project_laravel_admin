<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository extends BaseRepository
{
    public function model()
    {
        return Product::class;
    }

    public function search($dataSearch)
    {
        $name = $dataSearch['name'];

        $category = $dataSearch['category_id'];

        $minPrice = $dataSearch['min_price'];
        $maxPrice = $dataSearch['max_price'];


        return $this->model->withName($name)->withCategoryId($category)->withMinPrice($minPrice)->withMaxPrice($maxPrice)->latest('id')->paginate(5);
    }
}
