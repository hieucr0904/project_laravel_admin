<?php

namespace App\Http\Controllers;

use App\Http\Requests\Role\RoleRequest;
use App\Services\PermissionService;
use App\Services\RoleService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class RoleController extends Controller
{
    protected RoleService $roleService;

    protected PermissionService $permissionService;

    public function __construct(RoleService $roleService, PermissionService $permissionService)
    {
        $this->roleService = $roleService;

        $this->permissionService = $permissionService;

        View::share('permissions', $this->permissionService->getAll());

        View::share('permissionGroup', $this->permissionService->getWithGroup());
    }

    public function index(Request $request)
    {
        $roles = $this->roleService->getAllRole($request);

        return view('admin.roles.index', compact('roles'));
    }

    public function create()
    {
        return view('admin.roles.create');
    }

    public function store(RoleRequest $request)
    {
        $this->roleService->create($request);

        return redirect()->route('roles.index');
    }

    public function show($id)
    {
        $role = $this->roleService->getRoleId($id);

        return view('admin.roles.show', compact('role'));
    }

    public function edit($id)
    {

        $role = $this->roleService->getRoleId($id);

        $permissionIds = $this->roleService->getPermissionIdRole($id);

        return view('admin.roles.edit', compact('role', 'permissionIds'));
    }

    public function update(RoleRequest $request, $id)
    {
        $this->roleService->update($request, $id);

        return redirect()->route('roles.index');
    }

    public function destroy($id)
    {
        $this->roleService->delete($id);

        return redirect()->route('roles.index');
    }
}
