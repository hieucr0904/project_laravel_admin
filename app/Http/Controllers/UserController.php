<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Requests\User\UserRequest;
use App\Services\RoleService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class UserController extends Controller
{
    protected RoleService $roleService;

    protected UserService $userService;

    public function __construct(UserService $userService, RoleService $roleService)
    {
        $this->userService = $userService;

        $this->roleService = $roleService;

        View::share('roles', $this->roleService->getAll());
    }

    public function index(Request $request)
    {
        $users = $this->userService->getAllUser($request);

        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(UserRequest $request)
    {
        $this->userService->store($request);

        return redirect()->route('users.index');
    }

    public function show($id)
    {
        $user = $this->userService->getById($id);

        return view('admin.users.show', compact('user'));
    }

    public function edit($id)
    {
        $user = $this->userService->getById($id);

        $roleIds = $this->userService->getRoleIdUser($id);

        return view('admin.users.edit', compact('user', 'roleIds'));
    }

    public function update(UpdateUserRequest $request, $id)
    {
        $this->userService->update($request, $id);

        return redirect()->route('users.index');
    }

    public function destroy($id)
    {
        $this->userService->delete($id);

        return redirect()->route('users.index');
    }
}
