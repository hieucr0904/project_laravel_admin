<?php

namespace App\Traits;

use Intervention\Image\Facades\Image;

trait HandleImage
{
    protected $imageDefault = 'default.png';
    protected $path = 'uploads/products/';

    public function verifyImage($request)
    {
        return $request->hasFile('image') && $request->file('image')->isValid();
    }

    public function saveImage($request)
    {
        $image = $this->imageDefault;
        if ($this->verifyImage($request)) {
            $file = $request->file('image');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $location = $this->path . $filename;
            Image::make($file)->resize(300)->save($location);
            $image = $filename;
        }
        return $image;
    }

    public function updateImage($request, $currentImage)
    {

        if ($this->verifyImage($request)) {
            $this->deleteImage($currentImage);
            return $this->saveImage($request);
        }
        return $currentImage;
    }

    public function deleteImage($imageName)
    {
        $pathName = $this->path . $imageName;
        if (file_exists($pathName) && $imageName != $this->imageDefault) {
            unlink($pathName);
        }
    }
}
