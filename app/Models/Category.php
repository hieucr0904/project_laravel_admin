<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    // Accessor append into json or array
    protected $appends = ['parent_category_name'];

    protected $table = 'categories';

    protected $fillable = [
        'parent_id',
        'name'
    ];

    public function products()
    {
        return $this->hasMany(Product::class, 'category_id', 'product_id');
    }

    public function parentCategory()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'id');
    }

    // Define accessor
    public function getParentCategoryNameAttribute()
    {
        return $this->parentCategory ? $this->parentCategory->name : null;
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'like', '%' . $name . '%') : null;
    }
}
