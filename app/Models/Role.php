<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $table = 'roles';

    protected $fillable = [
        'name',
        'display_name'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function permissions()
    {
        return $this->belongsToMany(
            Permisson::class,
            'role_permission',
            'role_id',
            'permission_id'
        );
    }

    public function user()
    {
        return $this->belongsToMany(
            User::class,
            'role_user',
            'role_id',
            'user_id'
        );
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'like', '%' . $name . '%') : null;
    }

    public function attachPermission($permissionId)
    {
        return $this->permissions()->attach($permissionId);
    }

    public function syncPermission($permissionId)
    {
        return $this->permissions()->sync($permissionId);
    }

    public function syncUser($user_id)
    {
        return $this->user()->sync($user_id);
    }

    public function detachPermission()
    {
        return $this->permissions()->detach();
    }

    public function detachUser()
    {
        return $this->user()->detach();
    }

    public function getIdPermission($id)
    {
        return $this->permissions()->where('role_id', $id)->pluck('permission_id');
    }

    public function scopeWithoutSuperAdmin($query)
    {
        return $query->where('name', '!=', 'super_admin');
    }
}
