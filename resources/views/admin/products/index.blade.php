@extends('admin.layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Products Management</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">All Products</li>
                </ol>
                <div class="container-fluid px-4">
                    @hasPermission('product_create')
                    <a href="{{ route('products.create') }}" class="btn btn-success">Create
                        Product</a>
                    @endhasPermission
                    <div class="row justify-content-center">
                        <div class="row justify-content-center">
                            <form method="GET" class="col-md-5 m-md-3 d-flex">
                                <select name="category_id" class=" form-control col-md-2 mr-2"
                                        aria-label="Default select example">
                                    <option value="" selected>Category</option>
                                    @foreach($categories as $category)
                                        <option {{ $category->id == request('category_id') ? 'selected' : '' }}
                                                value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>

                                <input type="search" name="name" value="{{ request('name') }}" class="form-control"
                                       placeholder="search ...">

                                <div class="col-md-2">
                                    <input type="number" class="form-control" name="min_price" placeholder="Min..."
                                           value="{{ request('min_price') }}">
                                    <br>
                                    <input type="number" class="form-control" name="max_price" placeholder="Max..."
                                           value="{{ request('max_price') }}">
                                </div>

                                <button type="submit" class="btn btn-primary">
                                    search
                                </button>
                            </form>
                        </div>
                    </div>

                    <div class="row justify-content-center">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Image</th>
                                <th scope="col">Name</th>
                                <th scope="col">Category</th>
                                <th scope="col">Price</th>
                                <th scope="col">Description</th>
                                <th scope="col">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($products as $product)
                                <tr>
                                    <th scope="row">{{$product->id}}</th>
                                    <td>
                                        <img src="{{ asset('uploads/products/'.$product->image) }}" width="100px" height="100px" alt="">
                                    </td>
                                    <td>{{$product->name}}</td>
                                    @foreach($product->categories as $category)
                                        <td>{{$category->name}}</td>
                                    @endforeach
                                    <td>{{$product->price . " ". "VNĐ"}}</td>
                                    <td>{{$product->description}}</td>
                                    <td>
                                        @hasPermission('product_show')
                                        <a href="{{route('products.show',$product->id)}}" class="btn btn-primary"><i
                                                class="far fa-eye"></i></a>
                                        @endhasPermission
                                        @hasPermission('product_edit')
                                        <a href="{{ route('products.edit', $product->id) }}"
                                           class="btn btn-success"><i
                                                class="far fa-edit"></i></a>
                                        @endhasPermission
                                        @hasPermission('product_delete')
                                        <button type="submit" class="btn btn-danger btn-delete"
                                                onclick="deleteProduct({{$product->id}})"><i
                                                class="fas fa-trash-alt"></i></button>
                                        @endhasPermission
                                        <form action="{{ route('products.delete', $product->id) }}"
                                              id="form-{{$product->id}}" method="POST">
                                            @method('DELETE')
                                            @csrf
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <p class="font-weight-bold" style="text-align: center"> -- NOT FOUND DATA -- </p>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="container-fluid">
                            {{$products->links()}}
                        </div>
                    </div>
                </div>

            </div>
        </main>
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid px-4">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2021</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection
@push('scripts')
    <script>
        function deleteProduct(id) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    event.preventDefault();
                    document.getElementById('form-' + id).submit();
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            })
        }
    </script>
@endpush
