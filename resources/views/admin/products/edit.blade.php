@extends('admin.layouts.app')

@section('title', 'Edit Product')

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Update Product</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Product</li>
                </ol>

                <div class="container">
                    <div class="row justify-content-center">
                        <form action="{{ route('products.update', $product->id) }}" method="POST"
                              enctype="multipart/form-data">
                            @method('PUT')
                            @csrf
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Name</label>
                                    <input name="name" type="text" value="{{ $product->name }}" class="form-control"
                                           placeholder="Name...">
                                    @error('name')
                                    <div class="link-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <label class="form-label">Category</label>
                            <select class="form-select" name="category_id[]" aria-label="Default select example">
                                @foreach($categories as $category)
                                    <option @foreach($product->categories as $p)
                                            @if($category->id == $p->id)
                                            selected="selected"
                                            @endif
                                            @endforeach
                                            value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Price</label>
                                    <input name="price" type="text" value="{{ $product->price }}" class="form-control"
                                           placeholder="Price...">
                                    @error('price')
                                    <div class="link-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Description</label>
                                    <input name="description" type="text" value="{{ $product->description }}"
                                           class="form-control"
                                           placeholder="Description...">
                                    @error('description')
                                    <div class="link-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Image</label>
                                    <input name="image" id="image" type="file" value="{{ $product->image }}"
                                           class="form-control">
                                    @error('image')
                                    <div class="link-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <img id="showImage"
                                     src="{{ \Illuminate\Support\Facades\URL::to('uploads/products/' . $product->image) }}"
                                     style="width: 300px;">
                            </div>
                            <br>
                            <button type="submit" class="btn btn-success">Update Product</button>
                        </form>
                    </div>
                </div>

            </div>
        </main>
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid px-4">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2021</div>
                    <div>
                        <a href="#">Privacy Policy</a>w
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#image').change(function (e) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#showImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(e.target.files['0']);
            })
        })
    </script>
@endpush


