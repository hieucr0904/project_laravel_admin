@extends('admin.layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Update User</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">User</li>
                </ol>

                <div class="container">
                    <div class="row justify-content-center">
                        <form action="{{ route('users.update', $user->id) }}" method="POST" class="container"
                              id="form-{{$user->id}}">
                            @method('PUT')
                            @csrf
                            <div class="mb-3">
                                <label class="form-label">Name</label>
                                <input type="text" class="form-control" name="name" value="{{$user->name}}">
                                @error('name')
                                <div class="link-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Email</label>
                                <input autocomplete="off" type="text" class="form-control" name="email"
                                       value="{{$user->email}}">
                                @error('email')
                                <div class="link-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Birthday</label>
                                <input type="text" class="form-control" name="birthday"
                                       value="{{$user->birthday}}">
                                @error('birthday')
                                <div class="link-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Address</label>
                                <input type="text" class="form-control" name="address"
                                       value="{{$user->address}}">
                                @error('address')
                                <div class="link-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Phone number</label>
                                <input type="text" class="form-control" name="phone_number"
                                       value="{{$user->phone_number}}">
                                @error('phone_number')
                                <div class="link-danger">{{ $message }}</div>
                                @enderror
                            </div>


                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Gender</label>
                                    <div>
                                        <input type="radio" name="gender"
                                               value="0" {{ $user->gender == "0" ? 'checked' : '' }}> Male<br>
                                        <input type="radio" name="gender"
                                               value="1" {{ $user->gender == "1" ? 'checked' : '' }} > Female<br>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Password</label>
                                <input type="password" class="form-control" name="password"
                                       placeholder="Password...">
                                @error('password')
                                <div class="link-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Role</label>
                                <div class="form">
                                    @foreach($roles as $r)
                                        <div class="row">
                                            <div class="form-check">
                                                <input
                                                    {{ $roleIds->contains($r->id) ? 'checked' : '' }} class="form-check-input"
                                                    type="checkbox"
                                                    name="display_name_permission[]" value="{{ $r->id }}">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    {{ $r->display_name }}
                                                </label>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </form>
                        <button class="btn btn-success" type="submit" onclick="updateUser({{$user->id}})"
                                style="height: 50px;width: 200px">Update Role
                        </button>
                    </div>
                </div>

            </div>
        </main>
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid px-4">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2021</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection

@push('scripts')
    <script>
        function updateUser(id) {
            Swal.fire({
                title: 'Do you want to save the changes?',
                showDenyButton: true,
                showCancelButton: true,
                confirmButtonText: 'Save',
                denyButtonText: `Don't save`,
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    event.preventDefault();
                    document.getElementById('form-' + id).submit();
                    Swal.fire('Saved!', '', 'success')
                } else if (result.isDenied) {
                    event.preventDefault();
                    window.history.back();
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
        }
    </script>
@endpush
