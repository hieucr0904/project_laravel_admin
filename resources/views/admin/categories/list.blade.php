<div id="list" data-action="{{route('categories.list')}}">
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Parent Name</th>
            <th scope="col">Created Date</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @forelse($categories as $category)
            <tr>
                <th scope="row">{{$category->id}}</th>
                <td>{{$category->name}}</td>
                <td>
                    {{ $category->ParentCategoryName }}
                </td>
                @foreach($category->products as $product)
                    <td>{{ $product ? $product->count() : 0 }}</td>
                @endforeach
                <td>{{ $category->created_at ? $category->created_at->format('d-m-Y') . ", " . $category->created_at->diffForHumans() : '01-01-1990' }}</td>
                <td>
                    @hasPermission('category_edit')
                    <a href="" class="btn btn-success btn-edit" data-bs-toggle="modal" --}}
                       data-bs-target="#EditCategoryModal" data-action="{{route('categories.edit',$category->id)}}"
                       data-update="{{route('categories.update',$category->id)}}"><i
                            class="far fa-edit"></i></a>
                    @endhasPermission
                    @hasPermission('category_delete')
                    <button type="submit" class="btn btn-danger btn-delete" onclick="deleteCategory({{$category->id}})">
                        <i class="fas fa-trash-alt"></i></button>
                    @endhasPermission
                    <form action="{{ route('categories.destroy', $category->id)}}" id="form-{{$category->id}}"
                          method="POST"
                          style="display: none">
                        @method('DELETE')
                        @csrf
                    </form>
                </td>
            </tr>
        @empty
            <p class="font-weight-bold" style="text-align: center"> -- NOT FOUND DATA -- </p>
        @endforelse
        </tbody>
    </table>
    <div class="container-fluid">
        {{$categories->links()}}
    </div>
</div>


