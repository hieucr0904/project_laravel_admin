@extends('admin.layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Role Management</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">All Roles</li>
                </ol>
                <div class="container-fluid px-4">
                    @hasPermission('role_create')
                    <a href="{{ route('roles.create') }}" class="btn btn-success">Create New Role</a>
                    @endhasPermission
                    <div class="row justify-content-center">
                        <div class="col-md-4">
                            <form method="GET" class="m-md-3 d-flex">
                                <input type="search" name="name" value="{{ request('name') }}" class="form-control"
                                       placeholder="search ...">
                                <button type="submit" class="btn btn-primary">
                                    search
                                </button>
                            </form>
                        </div>
                    </div>

                    <div class="row justify-content-center">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Name</th>
                                <th scope="col">Permission</th>
                                <th scope="col">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($roles as $role)
                                <tr>
                                    <th scope="row">{{$role->id}}</th>
                                    <td>{{$role->display_name}}</td>
                                    <td>
                                        @foreach($role->permissions as $key=>$permission)
                                            <span style="font-size: 14px" class="text-black">{{ $permission->display_name }}
                                                {{ $key + 1 == $role->permissions->count() ? '.' : ',' }}
                                                {{--                                                {{ $role->permissions->count() }}--}}
                                            </span>
                                        @endforeach
                                    </td>

                                    <td>
                                        @hasPermission('role_show')
                                        <a href="{{route('roles.show',$role->id)}}" class="btn btn-primary"><i
                                                class="far fa-eye"></i></a>
                                        @endhasPermission
                                        @hasPermission('role_edit')
                                        <a href="{{route('roles.edit',$role->id)}}" class="btn btn-success"><i
                                                class="far fa-edit"></i></a>
                                        @endhasPermission
                                        @hasPermission('role_delete')
                                        <button type="submit" class="btn btn-danger btn-delete"
                                                onclick="deleteRole({{$role->id}})"><i
                                                class="fas fa-trash-alt"></i></button>
                                        @endhasPermission
                                        <form action=" {{ route('roles.delete', $role->id) }}" id="form-{{$role->id}}"
                                              method="POST">
                                            @method('DELETE')
                                            @csrf
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                    <p class="font-weight-bold" style="text-align: center"> -- NOT FOUND DATA -- </p>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="container-fluid">
                            {{$roles->links()}}
                        </div>
                    </div>
                </div>

            </div>
        </main>
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid px-4">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2021</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection

@push('scripts')
    <script>
        function deleteRole(id) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    event.preventDefault();
                    document.getElementById('form-' + id).submit();
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            })
        }
    </script>
@endpush
