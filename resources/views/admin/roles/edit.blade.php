@extends('admin.layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Update Role</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Role</li>
                </ol>

                <div class="container">
                    <div class="row justify-content-center">
                        <form action="{{route('roles.update',$role->id)}}" method="POST" class="container"  id="form-{{$role->id}}">
                            @method('PUT')
                            @csrf
                            <div class="mb-3">
                                <label class="form-label">Name</label>
                                <input type="text" class="form-control" name="name" value="{{$role->name}}">
                                @error('name')
                                <div class="link-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Display_Name</label>
                                <input type="text" class="form-control" name="display_name"
                                       value="{{$role->display_name}}">
                                @error('display_name')
                                <div class="link-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <input class="form-check-input select-all-permission" type="checkbox">
                                <label class="form-label">Permission</label>
                                <div class="row">
                                    <div class="form-check-inline">
                                        @foreach($permissionGroup as $group => $permission)
                                            <div class="col-md-3">
                                                <input class="form-check-input select-item" type="checkbox"
                                                       id="select-group-permission">
                                                <label class="form-label"><b>{{ $group }}</b></label>
                                                @foreach($permission as $permissionItem)
                                                    <div class="card-text">
                                                        <div class="form-check">
                                                            <input
                                                                {{ $permissionIds->contains($permissionItem->id) ? 'checked' : '' }} class="form-check-input"
                                                                type="checkbox"
                                                                name="display_name_permission[]"
                                                                value="{{ $permissionItem->id }}">
                                                            <label class="form-check-label" for="flexCheckDefault">
                                                                {{ $permissionItem->display_name }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </form>
                        <button class="btn btn-success" type="submit" onclick="updateRole({{$role->id}})"
                                style="height: 50px;width: 200px">Update Role</button>
                    </div>
                </div>

            </div>
        </main>
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid px-4">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2021</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection

@push('scripts')
    <script>
        function updateRole(id) {
            Swal.fire({
                title: 'Do you want to save the changes?',
                showDenyButton: true,
                showCancelButton: true,
                confirmButtonText: 'Save',
                denyButtonText: `Don't save`,
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    event.preventDefault();
                    document.getElementById('form-' + id).submit();
                    Swal.fire('Saved!', '', 'success')
                } else if (result.isDenied) {
                    event.preventDefault();
                    window.history.back();
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
        }
    </script>
@endpush
